//
//  main.c
//  Lab2
//
//  Created by xiaofei xie on 4/15/19.
//  Copyright © 2019 xiaofei xie. All rights reserved.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// construct Employee struct to store the information of Employee
// with the member field of ID, firstName, LastName
struct Employee
{
    int ID;
    char firstName[64];
    char lastName[64];
    int salary;
};

// construct a list of employee to store/minupulate the input/output
struct Employee employeeList[1024];

void swap(struct Employee employeeList[],int last,int low) {
    struct Employee temp;
    temp = employeeList[last];
    employeeList[last] = employeeList[low];
    employeeList[low] = temp;
}

// quickSort by ID
void helperByID(int low,int high) {
    int i;
    int last;
    if(low < high) {
        last=low;
        for(i=low + 1;i<=high;i++) {
            if (employeeList[i].ID < employeeList[low].ID){
                swap(employeeList,++last,i);
            }
        }
        
        swap(employeeList,last,low);
        
        helperByID(low,last-1);
        helperByID(last+1,high);
    }
}

void quickSortByID() {
    int size = 0;
    while (employeeList[size].ID > 0) {
        size++;
    }
    helperByID(0, size - 1);
}

// quickSort by Salary
void helperBySalary(int low,int high) {
    int i;
    int last;
    if(low<high) {
        last=low;
        for(i=low + 1;i<= high;i++) {
            if (employeeList[i].salary > employeeList[low].salary){
                swap(employeeList,++last,i);
            }
        }
        
        swap(employeeList,last,low);
        
        helperBySalary(low,last-1);
        helperBySalary(last+1,high);
    }
}

void quickSortBySalary() {
    int size = 0;
    while (employeeList[size].ID > 0) {
        size++;
    }
    helperBySalary(0, size - 1);
}

// read the small.txt file to employeeList
// once it is done, break
void readFile(){
    // inital ID and Salary value
    int ID = 0;
    int salary = 0;
    char firstName[64];
    char lastName[64];
    
    for(int i=0;i<1024;i++){
        if(read_int(&ID) == 0 && read_string(firstName) == 0
           && read_string(lastName) == 0 && read_int(&salary) == 0){
            employeeList[i].ID = ID;
            
            if (strcpy(employeeList[i].firstName,firstName) == NULL) {
                printf("Error happends when COPYING input First Name to employeeList");
            }
            
            if (strcpy(employeeList[i].lastName,lastName) == NULL) {
                printf("Error happends when COPYING input Last Name to employeeList");
            }
            employeeList[i].salary = salary;
        }else{
            break;
        }
    }
}

// print main menu of the project
void printMainMenu(){
    printf("***********************************\n");
    printf("Employee DateBase Menu:\n");
    printf("***********************************\n");
    printf("(1)Print The Database\n");
    printf("(2)Lookup By ID\n");
    printf("(3)Lookup By Last Name\n");
    printf("(4)Add Employee\n");
    printf("(5)Quit\n");
    printf("(6)Remove an employee\n");
    printf("(7)Update an employee's information\n");
    printf("(8)Print the M employees with the highest salaries\n");
    printf("(9)Find all employees with matching LAST name\n");
    printf("***********************************\n");
    printf("Please make your choice (digit bwteen 1 to 9):");
}

// print the welcome diamond page to the use
void drawPicture(){
    int n=3;
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n - i; j++) {
            printf(" ");
        }
        for (int k = 0; k < i*2 -1; k++) {
            printf("*");
        }
        printf("\n");
    }
    
    for (int i = n-1; i >= 1;i--) {
        for(int k = 1;k <= n-i;k++) {
            printf(" ");
        }
        for (int j = 1; j <= i*2 -1; j++) {
            printf("*");
        }
        printf("\n");
    }
    printf("Welcome to DateBase System:\n");
    printf("***********************************\n");
}


// tranverse the employeeList and print the employees to the page
void printEmployeeDatabase(){
    printf("\nFIISTNAME\tLASTNAME\t\tSALARY\t\tID\n");
    printf("***********************************************************\n");
    int count = 0;
    while (count < 1024) {
        if(employeeList[count].salary > 0){
            printf("%s", employeeList[count].firstName);
            printf("\t\t%-10s", employeeList[count].lastName);
            printf("\t\t%d", employeeList[count].salary);
            printf("\t\t%d\n", employeeList[count].ID);
            count++;
        }else{
            break;
        }
    }
    printf("The END\n");
    printf("***********************************************************\n");
    printf("The Employees Amount is %d\n", count);
}


// implement binarySearch to cut half to reduce search complexity
int binarySearch(int left,int right,int target){
    while (left <= right) {
        int mid = (right + left) / 2;
        int midValue = employeeList[mid].ID;
        if (target < midValue || employeeList[mid].ID == 0) {
            right = mid -1;
        } else if (target == midValue) {
            return mid;
        } else {
            left = mid + 1;
        }
    }
    return -1;
}

// use binarySearch to find the index of target element and print the employee
void lookupById(){
    printf("Please Enter the 6 Diget Employee Id: ");
    int employeeId = 0;
    scanf("%d",&employeeId);
    
    int index = binarySearch(0,1024,employeeId);
    
    if (index == -1) {
        printf("***********************************************************\n");
        printf("Sorry, Employee ID you entered is NOT FOUND\n");
        printf("***********************************************************\n");
    } else {
        printf("\nFIISTNAME\tLASTNAME\t\tSALARY\t\tID\n");
        printf("***********************************************************\n");
        
        printf("%s", employeeList[index].firstName);
        printf("\t\t%-10s", employeeList[index].lastName);
        printf("\t\t%d", employeeList[index].salary);
        printf("\t\t%d\n", employeeList[index].ID);
        
        printf("***********************************************************\n");
    }
}

// travese the employeeList and compare input lastName with the element in employeeList
// to find what we are searching
void lookupByLastName(){
    printf("Please Enter Employee Last Name : ");
    
    char lastName[64];
    scanf("%s",&lastName);
    
    int index = 0;
    while (index < 1024) {
        if(employeeList[index].salary == 0){
            printf("***********************************************************\n");
            printf("Sorry, Employee tLASTNAME you entered is NOT FOUND\n");
            printf("***********************************************************\n");
            break;
        } else {
            if(!strcmp(employeeList[index].lastName, lastName)){
                printf("\nFIISTNAME\tLASTNAME\t\tSALARY\t\tID\n");
                printf("***********************************************************\n");
                
                printf("%s", employeeList[index].firstName);
                printf("\t\t%-10s", employeeList[index].lastName);
                printf("\t\t%d", employeeList[index].salary);
                printf("\t\t%d\n", employeeList[index].ID);
                
                printf("***********************************************************\n");
                return;
            }
            index++;
        }
    }
}

// require user to enter what they want to add
// add the employ imformation(firstName, lastName, salary) to employeeList
void addEmplyee(){
    printf("Please enter the employee's first name : ");
    char firstName[64];
    scanf("%s",firstName);
    
    printf("Please enter the employee's last name : ");
    char lastName[64];
    scanf("%s",lastName);
    
    printf("Enter salary of employee (30000 to 150000): ");
    int salary = 0;
    scanf("%d",&salary );
    
    while(salary<30000 || salary>150000){
        printf("Invilad number for salary. Again, Enter salary of employee (30000 to 150000): ");
        scanf("%d",&salary );
    }
    
    int ID = 0;
    int count = 0;
    while (count < 1024) {
        if(employeeList[count].ID > 0){
            ID = employeeList[count].ID;
            count++;
        }else{
            break;
        }
    }
    
    // we assume the ID is auto-increase by 10
    ID += 10;
    
    printf("Do you want to add the new employee to the DataBase ?\n");
    
    printf("firstName: %s, ",lastName);
    printf("lastName:  %s, " ,lastName);
    printf("salary: %d, ", salary);
    printf("id: %d\n", ID);
    printf("Enter (1) for YES, (0) for NO : ");
    
    int doubleCheck;
    scanf("%d",&doubleCheck);
    if(doubleCheck != 1){
        return;
    } else {
        int index = 0;
        while (index < 1024) {
            if (employeeList[index].salary == 0) {
                strcpy(employeeList[index].firstName,firstName);
                strcpy(employeeList[index].lastName,lastName);
                employeeList[index].ID = ID;
                employeeList[index].salary = salary;
                break;
            }
            index++;
        }
    }
    printEmployeeDatabase();
}


void removeAnEmployee() {
    printf("Enter ID of the REMOVE employee (100000 to 999999): ");
    int employeeId = 0;
    scanf("%d",&employeeId );
    
    while(employeeId < 100000 || employeeId > 999999){
        printf("Invilad number for ID. Again, Enter ID of employee (100000 to 999999): ");
        scanf("%d",&employeeId );
    }
    
    int index = binarySearch(0,1024,employeeId);
    
    if(index != -1){
        
        printf("Comfirm to REMOVE the Employee ? \n");
        printf("firstName: %s, ",employeeList[index].firstName);
        printf("lastName:  %s, " ,employeeList[index].lastName);
        printf("salary: %d, ", employeeList[index].salary);
        printf("id: %d\n", employeeId);
        
        printf("Enter (1) for YES, (0) for NO : ");
        
        int doubleCheck;
        scanf("%d",&doubleCheck);
        // change the rest of element in array to front to avoid the hole
        if(doubleCheck == 1){
            while (employeeList[index].salary != 0) {
                employeeList[index] = employeeList[index + 1];
                index++;
            }
        }
        
        printEmployeeDatabase();
    } else {
        printf("***********************************\n");
        printf("NOT FOUND the employee with the ID\n");
        printf("***********************************\n");
    }
}

void updateEmployeeInformation() {
    printf("Enter ID of the REMOVE employee (100000 to 999999): ");
    int employeeId = 0;
    scanf("%d",&employeeId );
    
    while(employeeId < 100000 || employeeId > 999999){
        printf("Invilad number for ID. Again, Enter ID of employee (100000 to 999999): ");
        scanf("%d",&employeeId );
    }
    
    int index = binarySearch(0,1024,employeeId);
    
    if(index != -1){
        printf("Do you want to change employeeID ?\n");
        printf("Enter (1) for YES, (0) for NO : ");
        int IDchoice = 0;
        scanf("%d",&IDchoice);
        if (IDchoice == 1) {
            printf("Enter NEW employee ID (100000 to 999999): ");
            int employeeId = 0;
            scanf("%d",&employeeId );
            
            while(employeeId < 100000 || employeeId > 999999){
                printf("Invilad number for ID. Again, Enter ID of employee (100000 to 999999): ");
                scanf("%d",&employeeId );
            }
            
            employeeList[index].ID = employeeId;
        }
        
        printf("Do you want to change firstName ?\n");
        printf("Enter (1) for YES, (0) for NO : ");
        int firstNameChoice = 0;
        scanf("%d",&firstNameChoice);
        if (firstNameChoice == 1) {
            printf("Please enter NEW employee's first name : ");
            char firstName[64];
            scanf("%s",firstName);
            strcpy(employeeList[index].firstName, firstName);
        }
        
        printf("Do you want to change lastName ?\n");
        printf("Enter (1) for YES, (0) for NO : ");
        int lastNameChoice = 0;
        scanf("%d",&lastNameChoice);
        if (lastNameChoice == 1) {
            printf("Please enter NEW employee's last name : ");
            char lastName[64];
            scanf("%s",lastName);
            strcpy(employeeList[index].lastName, lastName);
        }
        
        printf("Do you want to change salary ?\n");
        printf("Enter (1) for YES, (0) for NO : ");
        int salaryChoice = 0;
        scanf("%d",&salaryChoice);
        if (salaryChoice == 1) {
            printf("Enter NEW employee salary (30000 to 150000): ");
            int salary = 0;
            scanf("%d",&salary );
            
            while(salary<30000 || salary>150000){
                printf("Invilad number for salary. Again, Enter salary of employee (30000 to 150000): ");
                scanf("%d",&salary );
            }
            
            employeeList[index].salary = salary;
        }
        // quick Sort By ID
        quickSortByID();
        
        printEmployeeDatabase();
    } else {
        printf("***********************************\n");
        printf("NOT FOUND the employee with the ID\n");
        printf("***********************************\n");
    }
}

void printEmployeesWithHighestSalaries() {
    int size = 0;
    while (employeeList[size].ID > 0) {
        size++;
    }
    
    printf("Enter M Number of the highest employees between 0 to %d : ", size);
    int number = 0;
    scanf("%d",&number );
    
    while(number < 0 || number > size){
        printf("Invilad number. Again, Enter Number of the highest employees (0 to %d): ", size);
        scanf("%d",&number );
    }
    
    // quick Sort By salary
    quickSortBySalary();
    
    printf("***********************************************************\n");
    printf("The %d employees with HIGHEST salary \n", number);
    printf("***********************************************************\n");
    printf("\nFIISTNAME\tLASTNAME\t\tSALARY\t\tID\n");
    printf("***********************************************************\n");
    int count = 0;
    while (count < number) {
        if(employeeList[count].salary > 0){
            printf("%s", employeeList[count].firstName);
            printf("\t\t%-10s", employeeList[count].lastName);
            printf("\t\t%d", employeeList[count].salary);
            printf("\t\t%d\n", employeeList[count].ID);
            count++;
        }else{
            break;
        }
    }
    printf("The END\n");
    printf("***********************************************************\n");
    
    // quick Sort By ID
    quickSortByID();
}

void findAllEmployeesByLastName() {
    printf("Please Enter Employee Last Name : ");
    
    char lastName[64];
    scanf("%s",&lastName);
    
    int index = 0;
    
    int size = 0;
    while (employeeList[size].ID > 0) {
        size++;
    }

    int found = 0;
    printf("\nFIISTNAME\tLASTNAME\t\tSALARY\t\tID\n");
    printf("***********************************************************\n");
    for (index = 0; index < size; index++) {
        if(!strcmp(employeeList[index].lastName, lastName)){
            found = 1;
            printf("%s", employeeList[index].firstName);
            printf("\t\t%-10s", employeeList[index].lastName);
            printf("\t\t%d", employeeList[index].salary);
            printf("\t\t%d\n", employeeList[index].ID);
        }
    }
    
    if (!found) {
        printf("\n");
        printf("Sorry, Employee LASTNAME you entered is NOT FOUND\n");
        printf("\n");
    }
}


/*交换数组中的两个元素*/


//read the file first and store all the information to employeeList
// according to the user choice to call the implemented founction
// 1 : printEmployeeDatabase();
// 2 : lookupById();
// 3 : lookupByLastName();
// 4 : addEmplyee();
// 5 : quit;
// 6 -- to Remove an employee;
// 7 -- to Update an employee's information;
// 8 -- Print the M employees with the highest salaries;
// 9 -- Find all employees with matching LAST name;

int main(int argc, char *argv[]) {
//    argc=2;
//    argv[0]="./workerDB";
//    argv[1]="/Users/xiaofeixie/Desktop/cs402-sp-lab2/cs402-sp-lab2/small.txt";
    
    if (open_file(argv[1]) == 0 && argc == 2) {
        readFile();
        
        // quick Sort By ID
        quickSortByID();
        
        int continueChoice = 0;
        drawPicture();
        printf("Continue to use System? ");
        printf("\n");
        printf("Enter (1) for YES, (0) for NO");
        printf("\n");
        printf("Please make choice : ");
        scanf("%d",&continueChoice);
        if (continueChoice != 1) {
            printf("***********************************\n");
            printf("Thanks For Using our System, Have a good day!\n");
            printf("***********************************\n");
            return 0;
        }
        
        int userChoice = 0;
        while(userChoice != 5) {
            printMainMenu();
            if (scanf("%d",&userChoice) != 0) {
                if (userChoice == 1) {
                    printEmployeeDatabase();
                } else if (userChoice == 2) {
                    lookupById();
                } else if (userChoice == 3) {
                    lookupByLastName();
                } else if (userChoice == 4) {
                    addEmplyee();
                } else if (userChoice == 5) {
                    printf("***********************************\n");
                    printf("Thanks For Using our System, Have a good day!\n");
                    printf("***********************************\n");
                    break;
                } else if (userChoice == 6) {
                    removeAnEmployee();
                } else if (userChoice == 7) {
                    updateEmployeeInformation();
                } else if (userChoice == 8) {
                    printEmployeesWithHighestSalaries();
                } else if (userChoice == 9) {
                    findAllEmployeesByLastName();
                } else {
                    printf("Your input is not what we expect, please input number between 1 to 9\n");
                }
            } else {
                printf("Error happend to your input, please input again \n");
            }
        }
        
        // close the file, in case of memory leak
        close_file();
        return 0;
    } else {
        printf("There is something WRONG when opening the file, whose name is : %s\n",argv[1]);
        return 0;
    }
}
